Sphinx IGWN Theme
=================

This repository contains the Sphinx_ theme for International Gravitational-wave
Observatory Network (IGWN) documentation projects. This repository is at the
same source for a Python package (built with Poetry_) that supplies default
Sphinx configuration values, and also a Cookiecutter_ template for
creating new documentation projects that use those defaults.

.. _Sphinx: https://www.sphinx-doc.org
.. _Poetry: https://python-poetry.org
.. _Cookiecutter: https://cookiecutter.readthedocs.io/
