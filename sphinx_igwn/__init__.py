import os.path


def setup(app):
    app.add_html_theme(__name__, os.path.abspath(os.path.dirname(__file__)) + '/theme')
